import merge from 'xtend';
import reducer from '../store/reducers/isLoadingReducer';
import {initialState} from '../store/reducers/isLoadingReducer';
import {IS_LOADING} from "../store/actions/actionTypes";

describe('isLoading reducer', () => {

    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    it('should handle IS_LOADING', () => {
        const action = {type: IS_LOADING, arg: true};
        const expectedState = merge(initialState,
            {isLoading: true});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

});