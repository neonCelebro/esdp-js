import merge from 'xtend';
import reducer from '../store/reducers/userReducer';
import {initialState} from '../store/reducers/userReducer';
import {
  CHANGE_PASS_FAILURE,
  CHECK_TOKEN_FAILURE,
  FACEBOOK_LOGIN_ERROR,
  FACEBOOK_LOGIN_SUCCESS,
  LOGIN_USER_FAILURE, LOGIN_USER_SUCCESS, LOGOUT_USER, LOGOUT_USER_FAILURE, RECOVER_PASSWORD_FAILURE,
  REGISTER_USER_FAILURE,
  REGISTER_USER_SUCCESS, VK_LOGIN_ERROR, VK_LOGIN_SUCCESS
} from "../store/actions/actionTypes";

describe('user reducer', () => {
  const userData = {
    email: 'user@mail.ru',
    role: 'user',
    sections: [1, 2, 3, 4],
    id: '555',
    displayName: 'lalala',
    facebookId: '123',
    vkontakteId: '123',
    token: '777'
  };

  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it('should handle REGISTER_USER_SUCCESS', () => {
    const action = {type: REGISTER_USER_SUCCESS, data: userData};
    const expectedState = merge(initialState, {registerError: null, user: userData});
    expect(reducer(initialState, action)).toEqual(expectedState);
  });

  it('should handle REGISTER_USER_FAILURE', () => {
    const action = {type: REGISTER_USER_FAILURE, data: {error: 'registerError'}};
    const expectedState = merge(initialState, {registerError: {error: 'registerError'}});
    expect(reducer(initialState, action)).toEqual(expectedState);
  });

  it('should handle LOGIN_USER_SUCCESS', () => {
    const action = {type: LOGIN_USER_SUCCESS, user: userData};
    const expectedState = merge(initialState, {user: userData, loginError: null});
    expect(reducer(initialState, action)).toEqual(expectedState);
  });

  it('should handle LOGIN_USER_FAILURE', () => {
    const action = {type: LOGIN_USER_FAILURE, data: {error: 'loginError'}};
    const expectedState = merge(initialState, {loginError: {error: 'loginError'}});
    expect(reducer(initialState, action)).toEqual(expectedState);
  });

  it('should handle LOGOUT_USER', () => {
    const action = {type: LOGOUT_USER};
    const expectedState = merge(initialState, {user: null});
    expect(reducer(initialState, action)).toEqual(expectedState);
  });

  it('should handle LOGOUT_USER_FAILURE', () => {
    const action = {type: LOGOUT_USER_FAILURE, data: {error: 'logoutError'}};
    const expectedState = merge(initialState, {logoutError: {error: 'logoutError'}});
    expect(reducer(initialState, action)).toEqual(expectedState);
  });

  it('should handle FACEBOOK_LOGIN_SUCCESS', () => {
    const action = {type: FACEBOOK_LOGIN_SUCCESS, data: userData};
    const expectedState = merge(initialState, {user: userData});
    expect(reducer(initialState, action)).toEqual(expectedState);
  });

  it('should handle FACEBOOK_LOGIN_ERROR', () => {
    const action = {type: FACEBOOK_LOGIN_ERROR, error: {fbRegisterError: 'fbRegisterError'}};
    const expectedState = merge(initialState, {registerError: {fbRegisterError: 'fbRegisterError'}});
    expect(reducer(initialState, action)).toEqual(expectedState);
  });

  it('should handle VK_LOGIN_SUCCESS', () => {
    const action = {type: VK_LOGIN_SUCCESS, data: userData};
    const expectedState = merge(initialState, {user: userData});
    expect(reducer(initialState, action)).toEqual(expectedState);
  });

  it('should handle VK_LOGIN_ERROR', () => {
    const action = {type: VK_LOGIN_ERROR, error: {vkLoginError: 'vkLoginError'}};
    const expectedState = merge(initialState, {registerError: {vkLoginError: 'vkLoginError'}});
    expect(reducer(initialState, action)).toEqual(expectedState)
  });

  it('should handle RECOVER_PASSWORD_FAILURE', () => {
    const action = {type: RECOVER_PASSWORD_FAILURE, data: {recoverPassError: 'recoverPassError'}};
    const expectedState = merge(initialState, {error: {recoverPassError: 'recoverPassError'}});
    expect(reducer(initialState, action)).toEqual(expectedState);
  });

  it('should handle CHECK_TOKEN_FAILURE', () => {
    const action = {type: CHECK_TOKEN_FAILURE, data: {checkTokenError: 'checkTokenError'}};
    const expectedState = merge(initialState, {error: {checkTokenError: 'checkTokenError'}});
    expect(reducer(initialState, action)).toEqual(expectedState);
  });

  it('should handle CHANGE_PASS_FAILURE', () => {
    const action = {type: CHANGE_PASS_FAILURE, data: {checkPassError: 'checkPassError'}};
    const expectedState = merge(initialState, {error: {checkPassError: 'checkPassError'}});
    expect(reducer(initialState, action)).toEqual(expectedState);
  });

});

