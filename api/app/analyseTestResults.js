
const analyseTestResults = (req) => {
    const data = convertDataToPlusesAndMinusesArrays(req.body.data);

    const result = [];

    result.push(analyseBySocialDesirability(data));
    result.push(analyseBySelfSatisfaction(data));
    result.push(analyseByBehavior(data));
    result.push(analyseByIntelligence(data));
    result.push(analyseBySituationAtSchool(data));
    result.push(analyseByAppearance(data));
    result.push(analyseByAnxiety(data));
    result.push(analyseByCommunication(data));
    result.push(analyseByHappiness(data));
    result.push(analyseByFamilySituation(data));
    result.push(analyseBySelfConfidence(data));

    return result;
};

module.exports = analyseTestResults;


const convertDataToPlusesAndMinusesArrays = (data) => {
    const result = {
        pluses: [],
        minuses: []
    };
    data.map(item => {
        switch(item.answer) {
            case 'Верно': case 'Скорее верно, чем неверно':
                result.pluses.push(item.id); break;
            case 'Неверно': case 'Скорее неверно, чем верно':
                result.minuses.push(item.id); break;
            default : break;
        }
    });

    return result
};


const analyseBySocialDesirability = (data) => {
    // let i = 0;
    const searchedPluses = [6, 24, 25, 42, 60, 62, 72];
    const searchedMinuses = [15, 53, 83];
    const score = howMuchSearchedNumbersIncludes(data, searchedPluses, searchedMinuses);
    let interpretation;
    if (score >= 7)
        interpretation = 'Результаты испытуемого могут быть искажены под влиянием сильной тенденции давать социально желательные ответы. В этом случае к результатам, полученным по шкале следует подходить с осторожностью и использовать их только как ориентировочные.';
    else interpretation = 'Результаты правдивы';

    return {
        title: 'Шкала социальной желательности',
        score,
        interpretation
    };
};

const analyseBySelfSatisfaction = data => {
    const searchedPluses = [
        2, 5, 10, 13, 17, 18, 19, 20, 21, 27, 28, 31, 32, 33, 34,
        37, 39, 40, 41, 44, 46, 47, 49, 54, 56, 58, 59, 61, 63, 65,
        66, 68, 71, 76, 78, 79, 80, 81, 82, 84, 86, 90
    ];
    const searchedMinuses = [
        1, 3, 4, 7, 8, 9, 11, 12, 14, 16, 22, 23, 26, 29, 30, 35, 36,
        38, 43, 45, 48, 50, 51, 52, 55, 57, 64, 67, 69, 70, 73, 74, 75,
        77, 85, 87, 88, 89
    ];
    const score = howMuchSearchedNumbersIncludes(data, searchedPluses, searchedMinuses);
    let interpretation;
    switch(score) {
        case score > 75 || score <= 19 :
            interpretation = 'Предельно высокий уровень (может свидетельствовать о защитно-высоком отношении к себе) или предельно низкий уровень самоотношения. Группа риска.'; break;
        case score >= 20 && score <=33 :
            interpretation = 'Низкий уровень, неблагоприятный вариант самоотношения';
            break;
        case (score >= 69 && score <= 75) || (score <= 45 && score >= 34) :
            interpretation = 'Средний уровень самоотношения'; break;
        case (score >= 46 && score <= 57) :
            interpretation = 'Высокий уровень, соответствующий социальному нормативу';
            break;
        default :
            interpretation = 'Очень высокий уровень самоотношения';
    }
    return {
        title: 'Общая удовлетворенность собой',
        score,
        interpretation
    };
};

const analyseByBehavior = data => {
    const searchedPluses = [13, 27, 39, 86];
    const searchedMinuses = [14, 16, 26, 29, 38, 64, 70, 77, 85];
    const score = howMuchSearchedNumbersIncludes(data, searchedPluses, searchedMinuses);
    let interpretation;
    if(score <= 4)
        interpretation = 'Подросток рассматривает  свое  поведение  как  не соответствующее требованиям взрослых.  Дополнительно  полезно  выявить отношение к    такому    поведению (оно    может характеризовать   негативистическое отношение к требованиям - “я веду себя  плохо  и  очень  этим доволен”; Подросток может переживать из-за того, что продолжает   выполнять   требования   взрослых (как маленький), и   относится   к   этому   отрицательно, возможны и оценка, соответствующая оценке взрослых. Однако следует иметь в виду, что суммарная оценка в общем ключе (Шкала социальной желательности) предполагает, что Соответствие требованиям взрослых рассматривается подростком как положительная черта';
    else if (score <= 9)
        interpretation = 'Обычно свидетельствует о реалистичном отношении к своему поведению.';
    else
        interpretation = 'Подросток оценивает свое поведение как соответствующее требованиям взрослых.  Дополнительно  полезно  выявить отношение    к    такому    поведению (оно    может характеризовать   негативистическое отношение к требованиям - “я веду себя  плохо  и  очень  этим доволен”; Подросток может переживать из-за того, что продолжает   выполнять   требования   взрослых (как маленький), и   относится   к   этому   отрицательно, возможны и оценка, соответствующая оценке взрослых. Однако следует иметь в виду, что суммарная оценка в общем ключе (Шкала социальной желательности) предполагает, что Соответствие требованиям взрослых рассматривается подростком как положительная черта';
    return {
        title: 'Поведение',
        score,
        interpretation
    };
};

const analyseByIntelligence = data => {
    const searchedPluses = [5, 10, 18, 19, 28, 34, 37, 47, 54, 66, 79, 80];
    const searchedMinuses = [23, 30, 75];
    const score = howMuchSearchedNumbersIncludes(data, searchedPluses, searchedMinuses);
    let interpretation;
    const samePart = 'Самооценку в этой области целесообразно сопоставить с   уровнем   реальной   успешности.   Важно   также проанализировать   отношение   подростка   к   своей школьной успешности';


    if(score <= 5)
        interpretation = 'Низкая самооценка интеллекта, школьной успешности. ' + samePart;
    else if (score <= 10)
        interpretation = 'Самооценка   интеллекта   и   школьной   успешности среднего уровня. ' + samePart;
    else interpretation = 'Высокая самооценка. ' + samePart;

    return {
        title: 'Интеллект, положение в школе',
        score,
        interpretation
    }
};

const analyseBySituationAtSchool = data => {
    const searchedPluses = [13, 31, 56];
    const searchedMinuses = [8, 11, 35, 50];
    const score = howMuchSearchedNumbersIncludes(data, searchedPluses, searchedMinuses);
    let interpretation;

    if(score <= 2)
        interpretation = 'Подросток   оценивает   школьную ситуацию как неблагоприятную. Школа вызывает у него неприязнь, тревогу, скуку.';
    else if(score <= 4)
        interpretation = 'Нейтральное отношение к школе.';
    else interpretation = 'Позитивное восприятие школьной ситуации';

    return {
        title: 'Ситуация в школе',
        score,
        interpretation
    }
};

const analyseByAppearance = data => {
    const searchedPluses = [17, 33, 44, 46, 61, 68, 78, 82];
    const searchedMinuses = [9, 73, 85];
    const score = howMuchSearchedNumbersIncludes(data, searchedPluses, searchedMinuses);

    let interpretation;
    if(score <= 3)
        interpretation = 'Низкая самооценка внешности, физических качеств. Низкая самооценка внешности –– фактор риска, она может быть причиной низкой общей самооценки подростка, его общей неудовлетворенности собой.';
    else if(score <= 7)
        interpretation = 'Средняя самооценка. Нередко это фактор риска и может быть причиной низкой общей самооценки подростка, его общей неудовлетворенности собой.';
    else
        interpretation = 'Высокая самооценка';

    return {
        title: 'Внешность, физическая привлекательность, физическое развитие как свойства, связанные с популярностью среди сверстников',
        score,
        interpretation
    }
};

const analyseByAnxiety = data => {
    const searchedPluses = [4, 7, 8, 11, 43, 55, 89];
    const searchedMinuses = [32, 41, 49, 84];
    const score = howMuchSearchedNumbersIncludes(data, searchedPluses, searchedMinuses);
    let interpretation;

    if(score <= 4)
        interpretation = 'высокий   уровень   эмоционального благополучия, Низкий (адаптивный) уровень тревожности';
    else if(score <= 7)
        interpretation = 'средний уровень тревожности';
    else
        interpretation = 'Высокий уровень тревожности. Подросток  нуждается  в психологической помощи.';

    return {
        title: 'Тревожность',
        score,
        interpretation
    }
};

const analyseByCommunication = data => {
    const searchedPluses = [17, 31, 37, 56, 58, 65, 66, 71, 76, 78];
    const searchedMinuses = [1, 3, 7, 12, 22, 45, 51, 55, 74];
    const score = howMuchSearchedNumbersIncludes(data, searchedPluses, searchedMinuses);

    let interpretation;
    const samePart = 'Следует  сопоставить  эти  данные  с  результатами объективного исследования положения школьника среди сверстников';
    if(score <= 6)
        interpretation = 'Низкая  самооценка популярности среди сверстников, умения общаться. Свидетельствует о неудовлетворенности потребности в общении у подростка.' + samePart;
    else if(score <= 13)
        interpretation = 'Средняя самооценка' + samePart;
    else
        interpretation = 'Высокая  самооценка  в общении, характеризующая переживание удовлетворенности  в этой сфере.' + samePart;

    return {
        title: 'Общение,популярность среди сверстников, умение общаться',
        score,
        interpretation
    }
};

const analyseByHappiness = data => {
    const searchedPluses = [2, 40, 44, 59, 90];
    const searchedMinuses = [50, 57, 67, 88];
    const score = howMuchSearchedNumbersIncludes(data, searchedPluses, searchedMinuses);

    let interpretation;

    if(score <= 2)
        interpretation = 'Переживание неудовлетворенности жизненной ситуацией.';
    else if(score <= 5)
        interpretation = 'Реалистичное отношение к жизненной ситуации.';
    else
        interpretation = 'Полное ощущение удовлетворенности жизнью.';

    return {
        title: 'Счастье и удовлетворенность',
        score,
        interpretation
    }
};

const analyseByFamilySituation = data => {
    const searchedPluses = [19, 81];
    const searchedMinuses = [16, 29, 36, 43, 67, 70];
    const score = howMuchSearchedNumbersIncludes(data, searchedPluses, searchedMinuses);
    let interpretation;

    if(score <= 2)
        interpretation = 'Подросток  не  удовлетворен своим. Фактор риска.';
    else if(score <= 5)
        interpretation = 'Средняя степень удовлетворенности.';
    else
        interpretation = 'Высокая степень удовлетворенности';

    return {
        title: 'Положение в семье',
        score,
        interpretation
    }
};

const analyseBySelfConfidence = data => {
    const searchedPluses = [5, 10, 18, 20, 21, 28, 39, 40, 63, 86, 90];
    const searchedMinuses = [9, 14, 48, 52, 69, 85, 87];
    const score = howMuchSearchedNumbersIncludes(data, searchedPluses, searchedMinuses);
    let interpretation;

    if(score <= 5)
        interpretation = 'Неуверенность в себе. Необходима психологическая помощь.';
    else if(score <= 15)
        interpretation = 'Средний уровень уверенности в себе, реалистичная самооценка.';
    else
        interpretation = 'Чрезмерно   высокий уровень уверенности в себе, чаще всего носит компенсаторно-защитный характер. Вместе с тем, особенно у младших подростков свидетельствовать об  инфантильном характере отношения к своим возможностям, недостаточной критичности. Необходима психологическая помощь.';

    return {
        title: 'Уверенность в себе',
        score,
        interpretation
    }
};

const howMuchSearchedNumbersIncludes = (analysedObject, searchedPluses, searchedMinuses) => {
    let number = 0;
    searchedPluses.forEach(item => {
        if(analysedObject.pluses.includes(item)) number++;
    });
    searchedMinuses.forEach(item => {
        if(analysedObject.minuses.includes(item)) number++;
    });
    return number;
};