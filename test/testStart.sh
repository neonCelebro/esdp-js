#!/bin/bash

CURRENT_DIR="$(pwd)"
FRONT_DIR=$CURRENT_DIR/../frontend
API_DIR=$CURRENT_DIR/../api
TESTS_DIR=$CURRENT_DIR

echo "====== Installing dependencies for frontend ======"
cd $FRONT_DIR
echo $(pwd)
yarn

echo " ====== Installing dependencies for API ======"
cd $API_DIR
echo $(pwd)
yarn

echo "====== Installing dependencies for tests ======"
cd $TESTS_DIR
echo $(pwd)
yarn

echo "====== Starting API ======"
cd $API_DIR
pm2 start "mongo" --name "mongo"
yarn seed:test
pm2 start "yarn test" --name "myapp-api"

echo "====== Starting frontend ======"
cd $FRONT_DIR
pm2 start "yarn start:test" --name "myapp-front"


echo "====== Running tests.... ======"
cd $TESTS_DIR
yarn start

echo "====== Stopping all servers ======"
pm2 kill





