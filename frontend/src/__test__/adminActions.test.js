import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import expect from 'expect';

import {
  addPsychologist, deletePsycho, editPsycho,
  fetchAllUsers,
  fetchFullInfoUser, fetchNotifications, fetchNotificationsAdmin, getImportantUsers, getOnePsycho, getPsychologists,
  notifyAdmin, unactiveNotifications
} from '../store/actions/adminActions';
import moxios from './moxios-config';
import {
  FETCH_ALL_USERS_ERROR,
  FETCH_ALL_USERS_SUCCESS, FETCH_FULL_INFO_USER_SUCCESS, FETCH_NOTIFICATIONS_ADMIN_SUCCESS, FETCH_NOTIFICATIONS_SUCCESS,
  GET_IMPORTANT_USERS,
  GET_PSYCHO_BY_ID,
  GET_PSYCHOLOGISTS,
  IS_LOADING,
  TOGGLE_RATEMODAL
} from "../store/actions/actionTypes";

const middleware = [thunk];
const mockStore = configureMockStore(middleware);

describe('admin actions', () => {
  moxios.install();
  moxios.uninstall();

  it('should fetch all users success', done => {
    const users = [{id: 1, username: 'user1', email: 'email1'}, {id: 2, username: 'user2', email: 'email2'}];
    moxios.expectData(users);
    const expectedActions = [
      {type: IS_LOADING, arg: true},
      {type: FETCH_ALL_USERS_SUCCESS, users},
      {type: IS_LOADING, arg: false}
    ];
    const store = mockStore({});
    store.dispatch(fetchAllUsers())
      .then(() => {
        const actions = store.getActions();
        expect(actions).toEqual(expectedActions);
        done();
      })
  });


  it('should fetch full info about user', done => {
    const user = {id: 1, username: 'user1', email: 'email1'};
    moxios.expectData(user);
    const expectedActions = [
      {type: IS_LOADING, arg: true},
      {type: FETCH_FULL_INFO_USER_SUCCESS, user},
      {type: IS_LOADING, arg: false}
    ];
    const store = mockStore({});
    store.dispatch(fetchFullInfoUser(user.id))
      .then(() => {
        const actions = store.getActions();
        expect(actions).toEqual(expectedActions);
        done();
      })
  });

  it('should notify admin when user has finished a section', done => {
    const finishedSection = ["sectionId"];
    moxios.expectData(finishedSection);
    const sectionId = 1;
    const expectedActions = [
      {type: TOGGLE_RATEMODAL, finishedSection},
      {payload: {args: ["/"], method: "push"}, type: '@@router/CALL_HISTORY_METHOD'}
    ];
    const store = mockStore({});
    store.dispatch(notifyAdmin(sectionId))
      .then(() => {
        const actions = store.getActions();
        expect(actions).toEqual(expectedActions);
        done();
      });
  });


  it('should fetch admin`s notifications', done => {
    const notifications = [{id: 1, title: 'title1', descr: 'descr1'}, {id: 2, title: 'title2', descr: 'descr2'}];
    moxios.expectData(notifications);
    const expectedActions = [
      {type: FETCH_NOTIFICATIONS_ADMIN_SUCCESS, notifications}
    ];
    const store = mockStore({});
    store.dispatch(fetchNotificationsAdmin())
      .then(() => {
        const actions = store.getActions();
        expect(actions).toEqual(expectedActions);
        done();
      });
  });

  it('should make admin`s notifications inactive', done => {
    moxios.expectData();
    const notificationsIds = ['id1', 'id2'];
    const store = mockStore({});
    store.dispatch(unactiveNotifications(notificationsIds))
      .then(() => done());
  });


  it('should get one psycho by his id', done => {
    const data = {id: 'id1', name: 'psychoName', email: 'psychoEmail'};
    const psychoId = data.id;
    moxios.expectData(data);
    const expectedActions = [
      {type: GET_PSYCHO_BY_ID, data}
    ];
    const store = mockStore({});
    store.dispatch(getOnePsycho(psychoId))
      .then(() => {
        const actions = store.getActions();
        expect(actions).toEqual(expectedActions);
        done();
      });
  });

  it('should get all psychologists', done => {
    const psycho = [{id: 'id1', name: 'psycho1'}, {id: 'id2', name: 'psycho2'}];
    moxios.expectData(psycho);
    const expectedActions = [
      {type: IS_LOADING, arg: true},
      {type: IS_LOADING, arg: false},
      {type: GET_PSYCHOLOGISTS, psycho}
    ];
    const store = mockStore({});
    store.dispatch(getPsychologists())
      .then(() => {
        const actions = store.getActions();
        expect(actions).toEqual(expectedActions);
        done();
      });
  });

  it('should add one psychologist', done => {
    const onePsycho = {name: 'psycho3', email: 'psychoEmail'};
    moxios.expectData();
    const expectedActions = [
      {type: IS_LOADING, arg: true},
      {type: IS_LOADING, arg: false},
      {type: IS_LOADING, arg: true}
    ];
    const store = mockStore({});
    store.dispatch(addPsychologist(onePsycho))
      .then(() => {
        const actions = store.getActions();
        expect(actions).toEqual(expectedActions);
        done();
      });
  });

  it('should edit one psychologist`s data', done => {
    const editedPsycho = {name: 'psycho3', email: 'psychoEmail'};
    moxios.expectData();
    const expectedActions = [
      {type: IS_LOADING, arg: true},
      {type: IS_LOADING, arg: false},
      {type: IS_LOADING, arg: true}
    ];
    const store = mockStore({});
    store.dispatch(editPsycho(editedPsycho))
      .then(() => {
        const actions = store.getActions();
        expect(actions).toEqual(expectedActions);
        done();
      });
  });

  it('should delete one psychologist', done => {
    const psycho = [{id: 'id3', name: 'psycho3', email: 'psychoEmail'}];
    const psychoId = psycho[0].id;
    moxios.expectData(psycho);
    const expectedActions = [
      {type: IS_LOADING, arg: true},
      {type: IS_LOADING, arg: false},
      {type: GET_PSYCHOLOGISTS, psycho}
    ];
    const store = mockStore({});
    store.dispatch(deletePsycho(psychoId))
      .then(() => {
        const actions = store.getActions();
        expect(actions).toEqual(expectedActions);
        done();
      });
  });

  it('should fetch all notifications', done => {
    const importantData = [{id: 'id1', title: 'title1', descr: 'descr1'}, {id: 'id2', title: 'title2', descr: 'descr2'}];
    const expectedActions = [{type: FETCH_NOTIFICATIONS_SUCCESS, importantData}];
    moxios.expectData(importantData);
    const store = mockStore({});
    store.dispatch(fetchNotifications())
      .then(() => {
        const actions = store.getActions();
        expect(actions).toEqual(expectedActions);
        done();
      });
  });

  it('should get all important users', done => {
    const data = [{id: 'id1', name: 'user1'}, {id: 'id2', name: 'user2'}];
    const expectedActions = [{type: GET_IMPORTANT_USERS, data}];
    moxios.expectData(data);
    const store = mockStore({});
    store.dispatch(getImportantUsers())
      .then(() => {
        const actions = store.getActions();
        expect(actions).toEqual(expectedActions);
        done();
      });
  });
});

