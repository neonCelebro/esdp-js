const express = require('express');
const request = require('request-promise-native');
const nanoid = require("nanoid");
const nodemailer = require('nodemailer');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const User = require('../models/User');
const config = require('../config');


const createRouter = () => {
    const router = express.Router();
    router.post('/addPsychologist', [auth, permit('admin')], async (req, res) => {
        try {
            const psychologist = await new User(req.body);
            psychologist.token = nanoid(10);
            await psychologist.save();

            const allPsycho = await User.find({role: 'psychologist'});
            res.send(allPsycho);
        } catch (error) {
            res.send({message: 'В данный момент невозможно добавить психолога!'})
        }
    });

    router.get('/', async (req, res) => {
        try {
            const psychologists = await User.find({role: 'psychologist'});
            res.send(psychologists);
        } catch (e) {
            res.status(400).send({message: 'Психологи не найдены!'});
        }
    });

    router.delete('/deletePsycho', [auth, permit('admin')], async (req, res) => {
        try {
            const id = req.query.id;
            const psycho = await User.findOne({_id: id});
            await psycho.remove();
            const allPsycho = await User.find({role: 'psychologist'});
            await res.send(allPsycho);
        } catch (e) {
            res.status(500).send({message: 'Невозможно удалить психолога в данный момент!'})
        }
    });

    router.get('/psychoId', async (req, res) => {
        try {
            const psycho = await User.findOne({_id: req.query.id});
            res.send(psycho);
        } catch (e) {
            res.send({message: 'Ошибка с загрузкой данных психолога!'})
        }
    });

    router.post('/editPsycho', [auth, permit('admin')], async (req, res) => {
        try {
            const id = req.body._id;
            const psycho = await User.findOne({_id: id});
            psycho.displayName = req.body.displayName;
            psycho.email = req.body.email;
            psycho.password = req.body.password;
            psycho.token = nanoid(10);
            await psycho.save();
            const allPsycho = await User.find({role: 'psychologist'});
            await res.send(allPsycho);
        } catch (e) {
            res.send({message: 'Невозможно изменить психолога в данный момент!'})
        }
    });

    return router;

};

module.exports = createRouter;