import merge from 'xtend';
import reducer from '../store/reducers/sectionsReducer';
import {initialState} from '../store/reducers/sectionsReducer';
import {
    ADD_NEW_SECTION_FAILURE,
    ADD_NEW_SECTION_SUCCESS, ADD_QUESTION_SUCCESS, EDIT_SECTION_FAILURE, FETCH_ALL_SECTIONS_SUCCESS,
    GET_FINISHED_SECTION_FAILURE,
    GET_FINISHED_SECTION_SUCCESS, SUCCESS_SETTED_RATING_SECTION_FAILURE,
    TOGGLE_SECTION_FORM
} from "../store/actions/actionTypes";

describe('sections reducer', () => {

    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    it('should handle ADD_NEW_SECTION_SUCCESS', () => {
        const action = {type: ADD_NEW_SECTION_SUCCESS, section: {title: 'section1', descr: 'descr1'}};
        const expectedState = merge(initialState,
            {showSectionForm: false, sections: [{title: 'section1', descr: 'descr1'}]});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle FETCH_ALL_SECTIONS_SUCCESS', () => {
        const action = {
            type: FETCH_ALL_SECTIONS_SUCCESS,
            allSections: [{title: 'section2', descr: 'descr2'}, {title: 'section3', descr: 'descr3'}]
        };
        const expectedState = merge(initialState,
            {sections: [{title: 'section2', descr: 'descr2'}, {title: 'section3', descr: 'descr3'}]});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle TOGGLE_SECTION_FORM', () => {
        const action = {type: TOGGLE_SECTION_FORM};
        const expectedState = merge(initialState, {showSectionForm: !initialState.showSectionForm});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle ADD_QUESTION_SUCCESS', () => {
        initialState.sections = [{id: 1, title: 'section1', descr: 'descr1'}, {
            id: 2,
            title: 'section2',
            descr: 'descr2'
        }];
        const action = {type: ADD_QUESTION_SUCCESS, section: {id: 1, title: 'newTitle', descr: 'newDescr'}};
        const expectedState = merge(initialState, {
            sections: [{id: 2, title: 'section2', descr: 'descr2'}, {
                id: 1,
                title: 'newTitle',
                descr: 'newDescr'
            }]
        });
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle GET_FINISHED_SECTION_SUCCESS', () => {
        const action = {type: GET_FINISHED_SECTION_SUCCESS, section: {id: 1, title: 'section1', descr: 'descr1'}};
        const expectedState = merge(initialState, {finishedSection: {id: 1, title: 'section1', descr: 'descr1'}});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle EDIT_SECTION_FAILURE', () => {
        const action = {type: EDIT_SECTION_FAILURE, error: 'EDIT_SECTION_FAILURE'};
        const expectedState = merge(initialState, {error: 'EDIT_SECTION_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle ADD_NEW_SECTION_FAILURE', () => {
        const action = {type: ADD_NEW_SECTION_FAILURE, error: 'ADD_NEW_SECTION_FAILURE'};
        const expectedState = merge(initialState, {error: 'ADD_NEW_SECTION_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle GET_FINISHED_SECTION_FAILURE', () => {
        const action = {type: GET_FINISHED_SECTION_FAILURE, error: 'GET_FINISHED_SECTION_FAILURE'};
        const expectedState = merge(initialState, {error: 'GET_FINISHED_SECTION_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle SUCCESS_SETTED_RATING_SECTION_FAILURE', () => {
        const action = {type: SUCCESS_SETTED_RATING_SECTION_FAILURE, error: 'SUCCESS_SETTED_RATING_SECTION_FAILURE'};
        const expectedState = merge(initialState, {error: 'SUCCESS_SETTED_RATING_SECTION_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

});

