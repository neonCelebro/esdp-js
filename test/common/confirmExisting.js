module.exports = function (element) {
  for (let i = 0; i < 9;){
    try {
      browser.pause(1000);
      const item = browser.element(element);
      return item.waitForExist(1000);
    }catch (e) {
      browser.pause(1000);
      i++;
    }
  }
};