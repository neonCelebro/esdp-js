import merge from 'xtend';
import reducer from '../store/reducers/psychologistsReducer';
import {initialState} from '../store/reducers/psychologistsReducer';
import {
    ADD_PSYCHO_FAILURE, DELETE_PSYCHO_FAILURE, EDIT_PSYCHO_FAILURE,
    GET_PSYCHO_BY_ID, GET_PSYCHO_BY_ID_FAILURE, GET_PSYCHOLOGISTS,
    GET_PSYCHOLOGISTS_FAILURE
} from "../store/actions/actionTypes";

describe('psychologists reducer', () => {

    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    it('should handle GET_PSYCHOLOGISTS', () => {
        const action = {type: GET_PSYCHOLOGISTS, psycho: [{user: '123', role: 'psychologist'}]};
        const expectedState = merge(initialState,
            {psychologists: [{user: '123', role: 'psychologist'}]});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle GET_PSYCHO_BY_ID', () => {
        const action = {type: GET_PSYCHO_BY_ID, data: [{user: '123', role: 'psychologist'}]};
        const expectedState = merge(initialState,
            {onePsycho: [{user: '123', role: 'psychologist'}]});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle GET_PSYCHOLOGISTS_FAILURE', () => {
        const action = {type: GET_PSYCHOLOGISTS_FAILURE, error: 'GET_PSYCHOLOGISTS_FAILURE'};
        const expectedState = merge(initialState,
            {error: 'GET_PSYCHOLOGISTS_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle GET_PSYCHO_BY_ID_FAILURE', () => {
        const action = {type: GET_PSYCHO_BY_ID_FAILURE, error: 'GET_PSYCHO_BY_ID_FAILURE'};
        const expectedState = merge(initialState,
            {error: 'GET_PSYCHO_BY_ID_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle ADD_PSYCHO_FAILURE', () => {
        const action = {type: ADD_PSYCHO_FAILURE, error: 'ADD_PSYCHO_FAILURE'};
        const expectedState = merge(initialState,
            {error: 'ADD_PSYCHO_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle EDIT_PSYCHO_FAILURE', () => {
        const action = {type: EDIT_PSYCHO_FAILURE, error: 'EDIT_PSYCHO_FAILURE'};
        const expectedState = merge(initialState,
            {error: 'EDIT_PSYCHO_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle DELETE_PSYCHO_FAILURE', () => {
        const action = {type: DELETE_PSYCHO_FAILURE, error: 'DELETE_PSYCHO_FAILURE'};
        const expectedState = merge(initialState,
            {error: 'DELETE_PSYCHO_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

});