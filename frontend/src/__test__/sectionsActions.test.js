import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import expect from 'expect';
import moxios from 'moxios';

import {addNewSection} from '../store/actions/sectionActions';
import {ADD_NEW_SECTION_SUCCESS} from "../store/actions/actionTypes";
import moxiosConfig from './moxios-config';


const middleware = [thunk];
const mockStore = configureMockStore(middleware);

describe('sections actions', () => {
    moxiosConfig.install();
    moxiosConfig.uninstall();

    it('should add new section', done => {
        const section = {id: 1, title: 'title1', descr: 'descr1'};

        moxios.wait(() => {
            const request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
                response: section,
            });
        });

        const expectedActions = [{type: ADD_NEW_SECTION_SUCCESS, section}];

        const store = mockStore({});

        store.dispatch(addNewSection())
            .then(() => {
                const actions = store.getActions();
                expect(actions).toEqual(expectedActions);
                done();
            })
    });


});

