const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');
const Section = require('./models/Section');
const Question = require('./models/Question');
const Review = require('./models/Review');
const Contact = require('./models/Contacts');
const Answer = require('./models/Answer');
const About = require('./models/About');

const testDb = 'test-db';
const devDb = 'psychology-db';


mongoose.connect(`${config.db.url}/${process.env.MODE === 'test' ? testDb : devDb}`, { useNewUrlParser: true });

const db = mongoose.connection;

const dropCollection = async (collectionName) => {
    try {
        await db.dropCollection(collectionName);
    } catch (e) {
        console.log(`Collection ${collectionName} did not present, skipping drop...`)
    }
};

const collections = ['users', 'sections', 'questions', 'answers', 'reviews',
    'adminnotifications', 'reviewnotifications', 'usernotifications', 'abouts', 'forbids', 'lastansweredquestionindexes'];

db.once('open', async () => {
    collections.forEach(collectionName => (
        dropCollection(collectionName)
    ));

    const [user, admin, psycho1, psycho2] = await User.create({
        email: 'user1@mail.ru',
        displayName: 'Обычный пользователь',
        facebookId: '123',
        vkontakteId: '123',
        password: 'user1',
        role: 'user',
        token: ''
    }, {
        email: 'user123@mail.ru',
        displayName: 'Обычный пользователь2',
        facebookId: '777',
        vkontakteId: '999',
        password: '123',
        role: 'user',
        token: ''
    }, {
        email: 'user2@mail.ru',
        displayName: 'Админ',
        facebookId: '132',
        vkontakteId: '310072015',
        password: 'user2',
        role: 'admin',
        token: ''
    }, {
        email: 'psycho1@gmail.com',
        displayName: 'psycho1',
        password: '123',
        role: 'psychologist',
        token: ''
    }, {
        email: 'psycho2@gmail.com',
        displayName: 'psycho2',
        password: '123',
        role: 'psychologist',
        token: ''
    });

    const about = await About.create({
        title: 'О нас',
        about: 'Каждый из нас был или находится в периоде подросткового возраста. Подростковый (пубертатный) период имеет свои особенности в силу происходящих в молодом организме гормональных изменений. Психологи INSIGHT составили Курс развития личности для подростков, который состоит из 4 модулей (Эмоции и здоровье, Самооценка и Образ тела, Стресс и конфликты, Целеполагание и навыки коммуникации) и охватывает самые актуальные вопросы и проблемы подростков. Курс показал свою эффективность при живом групповом проведении. \n' +
            'Поэтому мы решили разработать интерактивный сайт, на котором этот курс представлен. Этот Курс не заменяет полноценной психологической консультации, но может стать личным помощником для любого молодого человека от 13 и старше. Вы можете оценить эффективность Курса сами непосредственно, поскольку у вас есть возможность пройти диагностику до и после курса прямо на сайте.\n' +
            'Вся информация, получаемая нами, находится в режиме конфиденциальности. Подробнее вы можете узнать из Политики конфиденциальности и Условия пользования.\n' +
            'Мы планируем улучшать сайт и развивать его. Будем рады получить отзывы, замечания, пожелания по электронной почте: info@insight.com.kg \n' +
            'INSIGHT благодарит команду IT-специалистов, выпускников IT – Attractor School, которые разработали сайт на бесплатной основе. \n'
    });

    const [section1, section2] = await Section.create(
        {
            title: 'Section 1',
            isActive: true,
            image: '',
            description: 'Description for section #1',
            rate: [],
            questions: []
        }, {
            title: 'Section 2',
            isActive: true,
            image: '',
            description: 'Description for section #2',
            rate: [],
            questions: []
        });


    const [question1, question2, question3, question4, question6, question7, question8, question9] = await Question.create({
        title: 'Первый вопрос',
        isImportant: false,
        type: 'input',
        data: [],
        sectionId: section1._id
    }, {
        title: 'Второй вопрос с одним вариантом ответа',
        isImportant: false,
        type: 'radio',
        data: ['вариант 1', 'вариант 2'],
        sectionId: section1._id
    }, {
        title: 'Третий вопрос с несколькими вариантами ответа',
        isImportant: false,
        type: 'checkbox',
        importantAnswerVariant: ['Вариант 1'],
        data: ['Вариант 1', 'variant 2', 'variant 3', 'variant 4'],
        sectionId: section1._id
    }, {
        title: 'Четверый вопрос',
        isImportant: false,
        type: 'picture',
        data: [],
        sectionId: section1._id
    }, {
        title: 'Первый вопрос',
        isImportant: false,
        type: 'input',
        data: [],
        sectionId: section2._id
    }, {
        title: 'Второй вопрос с одним вариантом ответа',
        isImportant: false,
        type: 'radio',
        data: ['вариант 1', 'вариант 2'],
        sectionId: section2._id
    }, {
        title: 'Третий вопрос с несколькими вариантами ответа',
        isImportant: false,
        type: 'checkbox',
        data: ['Вариант 1', 'variant 2', 'variant 3', 'variant 4'],
        sectionId: section2._id
    }, {
        title: 'Четверый вопрос',
        isImportant: false,
        type: 'picture',
        data: [],
        sectionId: section2._id
    });


    const section = await Section.findOne({_id: section1._id});
    section.questions.push(question1._id, question2._id, question3._id, question4._id);
    await section.save();

    const sectionTwo = await Section.findOne({_id: section2._id});
    sectionTwo.questions.push(question6._id, question7._id, question8._id, question9._id);
    await sectionTwo.save();

    const [review1, review2] = await Review.create({
        author: psycho1._id,
        userId: user._id,
        sectionId: section1._id,
        review: 'This is test review-1 for user'
    }, {
        author: psycho2._id,
        userId: user._id,
        sectionId: section2._id,
        review: 'This is test review-2 for user'
    });

    const contacts = await Contact.create({
        phone: '0557-55-09-79',
        whatsapp: '0557-55-09-79',
        facebook: 'Facebook.com/insight.teens2',
        instagram: 'insight_teens',
        address: 'Какой-то адрес'
    });

    db.close();
})
;