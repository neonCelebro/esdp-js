const urls = require('../urls');
const findAndClick = require('../../common/findAndClick');
const findAndSetValue = require('../../common/findAndSetValue');
const confirm = require('../../common/confirmLoginOrRegister');
const tryClick = require('../../common/TryClick');
const trySetValue = require('../../common/trySetValue');
const confirmExisting = require('../../common/confirmExisting');


module.exports = function () {
    this.When(/^я создаю нового психолога$/, function () {
        tryClick("#hamburger");
        tryClick("#addNewPsycho");
        tryClick("#addPsycho");
        trySetValue("//input[@placeholder='Введите email']", 'psycho1@mail.com');
        trySetValue("//input[@placeholder='Введите имя']", 'psycho1');
        trySetValue("//input[@placeholder='Введите пароль']", '123');
        return tryClick("//span[text()=\"Добавить\"]");
    });

    this.Then(/^я вижу созданного психолога на странице$/, function () {
        return confirmExisting(`//p[text()[contains(., 'Данные психолога')]]`);
    });

    this.When(/^я редактирую психолога$/, function () {
        tryClick("#psycho");
        tryClick("#editPsycho");
        browser.clearElement("//input[@placeholder='Введите email']");
        trySetValue("//input[@placeholder='Введите email']", 'otherPsyholog@mail.com');
        browser.clearElement("//input[@placeholder='Введите имя']");
        trySetValue("//input[@placeholder='Введите имя']", 'otherPsyholog');
        browser.clearElement("//input[@placeholder='Введите пароль']");
        trySetValue("//input[@placeholder='Введите пароль']", '555');
        return tryClick(`//span[text()='Сохранить']`);
    });

    this.Then(/^я вижу отредактированного психолога на странице$/, function () {
        return confirmExisting(`//p[text()[contains(., 'Данные психолога')]]`);
    });


    this.Given(/^я нажимаю на кнопку "([^"]*)"$/, function (buttonName) {
        tryClick('//span[text()="'+buttonName+ '"]')
    });

    this.Given(/^открывается модальное окно$/, function () {
        return browser.element('.AddingSection-container-24');
    });


    this.Given(/^я создаю секции с названием "([^"]*)" и описанием "([^"]*)", нажимаю на кнопку "([^"]*)"$/, function (sectionTitle, sectionDescription, buttonName) {
        const title = browser.isExisting(`//p[text()='${sectionTitle}']`);
        const description = browser.isExisting(`//p[text()='${sectionDescription}']`);

        if (!title && !description) {
            const data = [
                {field: `addingSectionTitle`, value: sectionTitle},
                {field: `addingSectionDescription`, value: sectionDescription}
            ];

            data.map(item => {
                return findAndSetValue('input', 'id', item.field, item.value);
            });

            return findAndClick('span', 'text', buttonName);
        }
    });

    this.Then(/^я вижу созданную секцию с названием "([^"]*)" и описанием "([^"]*)" на странице$/, function (sectionTitle, sectionDescription) {
        return browser.element(`//p[text()='${sectionDescription}']`);
    });


    this.Given(/^я вхожу на сайт как "([^"]*)" c поролем "([^"]*)"$/, function (user, pass) {
        browser.url(urls.loginUrl);
        const loginData = [
            {field: '#emailForm', value: user},
            {field: '#passwordForm', value: pass}
        ];

        loginData.map(item => {
            return trySetValue(item.field, item.value);
        });

        tryClick("//span[text()='Войти']");
        return browser.pause(3000);
    });

    this.Given(/^я открываю секцию$/, function () {

        return findAndClick('div', 'id', 'TestSection');
    });

    this.Given(/^я выбираю тип "([^"]*)"$/, function (questionType) {
        tryClick("#TestSection");
        tryClick("#TestSection #SectionSelect");
        return tryClick("//*[@id=\"" + questionType + "\"]");
    });

    this.When(/^ввожу текст и добавляю вопрос$/, function () {
        if (browser.isExisting("//h2[text()=\"Форма добавления простого вопроса\"]")) {
            trySetValue("//*[@id=\"title\"]", "simple");
            return tryClick("#addQuestion");
        }
        if (browser.isExisting("//h2[text()=\"Форма добавления вопроса с одним вариантом ответа\"]") ||
            browser.isExisting("//h2[text()=\"Форма добавления вопроса с несколькими вариантами ответа\"]")) {
            trySetValue("//*[@id=\"title\"]", "вопрос с одним вариантом ответа");
            tryClick("//span[text()=\"Добавить вариант\"]");
            trySetValue("//*[@id=\"optionText\"]", "вариант 1");
            tryClick("#addOption");
            return tryClick("#addQuestion");
        }
        if (browser.isExisting("//h2[text()=\"Форма добавления вопроса с типом рисунок\"]")) {
            trySetValue("//*[@id=\"title\"]", "нарисуй свой дом");
            return tryClick("#addQuestion");
        }
        if (browser.isExisting("//h2[text()=\"Выберите файл в формате csv\"]")) {
            browser.chooseFile("#flat-button-file", "testDb.csv");
            return tryClick("//span[text()=\"Далее\"]");
        }
    });

    this.When(/^добавляю картинку$/, function () {
        findAndClick('div', 'title', 'Image');
        const isExisting = browser.isExisting(`//div[@class='rdw-image-modal']`);
        if (isExisting) {
            findAndSetValue('input', 'name', 'imgSrc', 'http://bipbap.ru/wp-content/uploads/2017/10/0_8eb56_842bba74_XL-220x220.jpg');
            findAndClick('button', 'text', 'Add');
        }

        return findAndClick('button', 'id', 'addQuestion');
    });

    this.Then(/^закрывается модальное окно и я вижу созданный вопрос в секции$/, function () {
        return browser.element(`//p[text()[contains(., 'Простой вопрос') and 
        contains(., 'Вопрос с одним вариантом ответа') and 
        contains(., 'Вопрос с несколькими вариантами ответа') and
        contains(., 'Тест') and
        contains(., 'Блок с информацией') ]]`, 2000);
    });

    this.When(/^я захожу на страницу добавления психолога$/, function () {
        tryClick(`//button[@id='hamburger']`);
        return tryClick(`//ul[@id="addPsycho"]`);
    });

    this.When(/^я нажимаю на кнопку удаления психолога$/, function () {
        tryClick("//*[@id=\"psycho\"]/div[1]");
        return tryClick(`//span[text()="Удалить"]`);
    });

    this.Then(/^я вижу что психолог был удален$/, function () {
        return confirmExisting(`//h4[text()='Психолог удален!']`);
    });

    this.When(/^я нажимаю на кнопку удаления вопроса/, function () {
        return tryClick("#TestSection #delete");
    });

    this.Then(/^вопрос удаляется из секции$/, function () {
        return browser.isExisting("//p[text()=\"simple\"]") === false;
    });

    this.Given(/^я нажимаю на кнопку редактирования вопроса$/, function () {
        return tryClick("#TestSection #edit");
    });

    this.When(/^я ввожу текст в поле ввода$/, function () {
        browser.clearElement("#editText");
        trySetValue("#editText", "новый текст");
        return tryClick("#editButton");
    });

    this.Then(/^закрывается модальное окно и я вижу отредактированный вопрос в секции$/, function () {
        return browser.isExisting(`//p[text()[contains(., 'новый текст')]]`);
    });

    this.When(/^я ввожу свои данные$/, function () {
        const data = [
            {field: '#password', value: 'user1'},
            {field: '#name', value: "Обычный пользователь2"},
            {field: '#email', value: 'simpleUser2@mail.ru'}
        ];

        data.forEach(item => {
            return trySetValue(item.field, item.value);
        });

        return tryClick("//*[@id=\"root\"]/div/main/div/form/div[4]/div/button");
    });

    this.Then(/^я вижу сообщение об успешной регистрации$/, function () {
        return confirm('Вы успешно зарегистрировались!');
    });

    this.When(/^я нажимаю на кнопку смены статуса секции$/, function () {
        return tryClick("//*[@id=\"TestSection\"]/div/div[1]/div[1]/span/span[1]/span[1]/input");
    });

    this.When(/^я снова нажимаю на кнопку смены статуса$/, function () {
        return tryClick("//*[@id=\"TestSection\"]/div/div[1]/div[1]/span/span[1]/span[1]/input");
    });

    this.Then(/^статус секции меняется на выключенную$/, function () {
        return confirmExisting(`//input[name()[contains(., "switch")]]`);
    });

    this.When(/^я в первый раз захожу на сайт$/, function () {
        return browser.url(urls.mainUrl);
    });


    this.When(/^при регистрации я принимаю Пользовательское соглашение$/, function () {
        browser.url(urls.registerUrl);
        return tryClick("/html/body/div[5]/div[2]/div[2]/label/span[1]/span[1]/input")

    });

    this.Then(/^только тогда я смогу приступить к регистрации$/, function () {
        return confirmExisting('//input[name="email"]');
    });

    this.Then(/^я вижу сообщение об успешном входе$/, function () {
        return confirm('Вход выполнен');
    });

    this.Then(/^я вижу уведомления о новых рецензиях$/, function () {
        confirmExisting(`//div[contains(@class, "message")]`)
    });

    this.When(/^я перехожу на страницу контактов$/, function () {
        tryClick(`//button[@id="hamburger"]`);
        return tryClick(`//ul[@id="contacts"]`);
    });

    this.Then(/^я вижу список контактов компании$/, function () {
        return confirmExisting(`//div[contains(@class, "panel")]`);
    });

    this.When(/^изменяю контактные данные$/, function () {
        tryClick("#addContact");
        findAndSetValue('input', 'id', 'phone', '0552555000');
        findAndSetValue('input', 'id', 'whatsapp', '0552555666');
        findAndSetValue('input', 'id', 'fb', 'Facebook.com/insight.teens2');
        findAndSetValue('input', 'id', 'inst', 'insight_teens');
        findAndSetValue('input', 'id', 'address', 'Рандомный адресс');
        return tryClick(`//span[text()='Сохранить']`);
    });

    this.Then(/^я вижу что контакты были изменены$/, function () {
        return confirmExisting(`//div[contains(@class, "panel")]`);
    });
    this.Then(/^я разлогинен/, function () {
        return tryClick(`#logout`);
    });

    this.Then(/^я вижу всех пользователей$/, function () {
        return confirmExisting(`//div[contains(@class, "ImportantNotifications")]`);
    });

    this.When(/^я регистрируюсь$/, function () {
        const data = [
            {field: '#password', value: '123'},
            {field: '#name', value: "kairat"},
            {field: '#email', value: 'lolka@mail.com'}
        ];

        data.forEach(item => {
            return trySetValue(item.field, item.value);
        });

        return tryClick("//*[@id=\"root\"]/div/main/div/form/div[4]/div/button");
    });

    this.When(/^я изменяю пароль$/, function () {
        tryClick("//*[@id=\"hamburger\"]");
        tryClick("//span[text()='Сменить пароль']");
        trySetValue("//*[@id=\"oldPassForm\"]", "123");
        trySetValue("//*[@id=\"passwordForm\"]", "555");
        trySetValue("//*[@id=\"repeatPassForm\"]", "555");
        return tryClick("//*[@id=\"root\"]/div/main/form/div[4]/div/button");
    });

    this.When(/^я регистрируюсь как новый пользователь$/, function () {
        browser.url(urls.registerUrl);

        const data = [
            {field: '#email', value: 'kairat@mail.com'},
            {field: '#name', value: 'kairat'},
            {field: '#password', value: '123'}
        ];

        data.map(item => {
            return trySetValue(item.field, item.value);
        });

        return tryClick(`//span[text()='Зарегистрироваться']`);
    });

    this.When(/^прохожу секцию$/, function () {
        tryClick(`//span[text()='Пройти секцию']`);
        trySetValue(`#root > div > main > div > div > div:nth-child(1) > div > div > div > div > p > div > form > div > div > div > input`, 'Ответ1');
        tryClick(`//span[text()="Далее"]`);
        tryClick(`//input[contains(@type, "radio")]`);
        tryClick(`//span[text()="Далее"]`);
        tryClick(`//input[contains(@value, "Вариант 1")]`);
        tryClick(`//span[text()="Далее"]`);
        confirmExisting('//canvas[contains(@width, "800")]');
        for (var i = 0; i < 10; i++) {
            browser.leftClick(`//canvas[contains(@width, "800")]`, i, 100);
        }
        tryClick(`//span[text()="Завершить"]`);
        trySetValue(`//textarea[@name="comment"]`, 'Оценка 1');
        tryClick('/html/body/div[7]/div[2]/div/div/div[3]/div/span[2]/span[3]');
        return tryClick(`//button[text()="Далее"]`);
        // return tryClick(`//input[@id="loggedIn"]`);
    });

    this.Then(/^я вижу уведомление что пароль был изменен$/, function () {
        return confirmExisting(`//div[contains(@class, "message")]`)
    });

    this.When(/^перехожу на страницу со всеми пользователями с важными ответами$/, function () {
        return tryClick(`//button[@id='important']`);
    });

    this.When(/^я написал рецензию для секции$/, function () {
        tryClick(`//button[@id='done']`);
        browser.refresh();
        tryClick(`//span[text()="Подробнее"]`);
        trySetValue(`//textarea[contains(@placeholder, "Введите текст рецензии")]`, 'Рецензия');
        return tryClick(`//span[text()="Отправить рецензию"]`);
    });

    this.Then(/^я вижу уведомление что рецензия была отправлена$/, function () {
        return confirmExisting(`//div[contains(@class, "message")]`);
    });

    this.When(/^перехожу на роут с законченными секциями,$/, function () {
        return browser.url(urls.finishedSections);
    });

    this.Then(/^я вижу имя пользователя завершившего секцию$/, function () {
        return confirmExisting('//h3[text()="Обычный пользователь2"]');
    });

    this.When(/^я нажимаю кнопку "([^"]*)"$/, function (buttonName) {
        return tryClick(`//span[text()=${buttonName}]`);
    });

    this.Then(/^я вижу ответы пользователя$/, function () {
      return confirmExisting("//h2[text()=\"Вопросы и ответы\"]");
    });

    this.When(/^я перехожу на роут со всеми пользователями$/, function () {
        return browser.url(urls.allUsers);
    });

    this.When(/^нажимаю на карточку пользователя$/, function () {
        return tryClick('//span[text()="Обычный пользователь2"]');
    });

    this.Then(/^я вижу информацию об этом пользователе$/, function () {
        return confirmExisting('//span[text()="Имя: Обычный пользователь2"]');
    });

    this.When(/^я перехожу на страницу О нас$/, function () {
        tryClick(`//button[@id='hamburger']`);
        return tryClick(`//a[text()='О нас']`);
    });

    this.Then(/^я вижу информацию о проекте$/, function () {
        return confirmExisting(`//div[contains(@class, "area")]`);
    });

    this.When(/^я редактирую данные$/, function () {
        tryClick(`//span[text()="Редактировать"]`);
        browser.clearElement(`//input[@id='title']`);
        trySetValue(`//input[@id='title']`, 'About us');
        browser.clearElement(`//textarea[@id='aboutArea']`);
        trySetValue(`//textarea[@id='aboutArea']`, 'some info about us');
        return tryClick(`//span[text()="Сохранить"]`);
    });

    this.Then(/^я вижу что данные были отредактированы$/, function () {
        return confirmExisting(`//p[text()="some info about us"]`)
    });

    this.When(/^я перехожу на страницу Политика конфиденциальности$/, function () {
        tryClick(`//button[@id='hamburger']`);
        return tryClick(`//a[text()='Политика конфиденциальности']`);
    });

    this.Then(/^я вижу информацию о политике конфиденциальности$/, function () {
        return confirmExisting(`//div[contains(@class, "infoArea")]`);
    });

    this.When(/^я удаляю секцию$/, function () {
        return tryClick(`//span[text()='Удалить']`);
    });

    this.Then(/^я вижу что секция была успешно удалена$/, function () {
        return confirmExisting(`//div[contains(@class, "message")]`);
    })

};