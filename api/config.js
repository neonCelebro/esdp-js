const path = require('path');

const rootPath = __dirname;

const testDb = 'test-db';
const devDb = 'psychology-db';

let config = {
    rootPath,
    uploadPath: path.join(rootPath, '/public/uploads'),
    db: {
        url: 'mongodb://localhost:27017',
        name: process.env.MODE === 'test' ? testDb : devDb
    },
    facebook: {
        appId: "1542982702494415",
        appSecret: "310534c44d1dc455c3aa01c9c5287db4"
    },
    vkontakte: {
        appId: '6684357',
        appSecret: 'Denh3EvRGcQ6ZXX1zvIS',
        redirectUrl: process.env.MODE === 'production' ? 'https://psyhology.ddns.net' : '//localhost:3000'
    }
};

module.exports = config;

