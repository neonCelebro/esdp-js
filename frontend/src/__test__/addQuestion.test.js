import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import moxios from 'moxios';
import expect from 'expect';

import {addQuestion, addQuestionSuccess} from "../store/actions/questionActions";
import {ADD_QUESTION_SUCCESS} from "../store/actions/actionTypes";
import axios from "../axios";

let middleware = [thunk];
const mockStore = configureMockStore(middleware);


describe('the whole addQuestion action', () => {

    beforeEach(function () {
        moxios.install(axios);
    });

    afterEach(function () {
        moxios.uninstall(axios);
    });


    it('should dispatch addQuestionSuccess after successfully sending ready question', (done) => {
        const sectionMock = {section: 'some info'};
        const question = {title: 'simple question'};

        moxios.wait(() => {
            const request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
                response: sectionMock,
            });
        });

        const expectedActionsArray = [
            {type: ADD_QUESTION_SUCCESS, section: sectionMock},
        ];

        const store = mockStore({});

         return store.dispatch(addQuestion(question)).then(() => {
            expect(store.getActions()).toEqual(expectedActionsArray);
            done();
         })


    });


    it('calls action creator addQuestionSuccess', () => {
        let section = {section: 'someInfo'};
        const addQuestionSuccessAction = addQuestionSuccess(section);

        expect(addQuestionSuccessAction).toEqual({type: ADD_QUESTION_SUCCESS, section: {section: 'someInfo'}});
    })
});








