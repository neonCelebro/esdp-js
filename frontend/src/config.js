let config = {
    apiUrl: 'http://localhost:8000/api/',
    facebookAppId: '1542982702494415',
    vkId: '6684357',
    imageUrl: 'http://localhost:8000/uploads/'
};

const env = process.env.REACT_APP_ENV || 'development';

switch (env) {
    case 'test':
        config.apiUrl = 'http://localhost:8010/api/';
        break;
    case 'production':
        config.apiUrl = '/api/';
        config.imageUrl= 'https://psyhology.ddns.net/uploads';
        break;
    default:
}

export default config;