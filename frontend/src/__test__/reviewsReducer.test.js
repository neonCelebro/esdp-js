import merge from 'xtend';
import reducer from '../store/reducers/reviewsReducer';
import {initialState} from '../store/reducers/reviewsReducer';
import {
    FETCH_ALL_REVIEWS_FAILURE, FETCH_ALL_REVIEWS_SUCCESS, FETCH_PSYCHOLOGIST_REVIEWS_FAILURE,
    FETCH_PSYCHOLOGIST_REVIEWS_SUCCESS, GET_USER_NOTIFICATIONS_FAILURE, GET_USER_NOTIFICATIONS_SUCCESS,
    SEND_REVIEW_FAILURE
} from "../store/actions/actionTypes";


describe('reviews reducer', () => {

    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    it('should handle FETCH_ALL_REVIEWS_SUCCESS', () => {
        const action = {type: FETCH_ALL_REVIEWS_SUCCESS, allReviews: [{author: '123', userId: 'abc', sectionId: '456', review: ''}]};
        const expectedState = merge(initialState,
            {reviews: [{author: '123', userId: 'abc', sectionId: '456', review: ''}]});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle FETCH_ALL_REVIEWS_FAILURE', () => {
        const action = {type: FETCH_ALL_REVIEWS_FAILURE, error: 'FETCH_ALL_REVIEWS_FAILURE'};
        const expectedState = merge(initialState, {error: 'FETCH_ALL_REVIEWS_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle FETCH_PSYCHOLOGIST_REVIEWS_SUCCESS', () => {
        const action = {type: FETCH_PSYCHOLOGIST_REVIEWS_SUCCESS, psychologistReviews: [{author: '123', userId: 'abc', sectionId: '456', review: 'Review from psychologist'}]};
        const expectedState = merge(initialState,
            {psychologistReviews: [{author: '123', userId: 'abc', sectionId: '456', review: 'Review from psychologist'}]});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle FETCH_PSYCHOLOGIST_REVIEWS_FAILURE', () => {
        const action = {type: FETCH_PSYCHOLOGIST_REVIEWS_FAILURE, error: 'FETCH_PSYCHOLOGIST_REVIEWS_FAILURE'};
        const expectedState = merge(initialState,
            {error: 'FETCH_PSYCHOLOGIST_REVIEWS_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle GET_USER_NOTIFICATIONS', () => {
        const action = {type: GET_USER_NOTIFICATIONS_SUCCESS, data: [{user: 'abc', questionId: '123', sectionId: '456'}]};
        const expectedState = merge(initialState,
            {notifications: [{user: 'abc', questionId: '123', sectionId: '456'}]});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle GET_USER_NOTIFICATIONS_FAILURE', () => {
        const action = {type: GET_USER_NOTIFICATIONS_FAILURE, error: 'GET_USER_NOTIFICATIONS_FAILURE'};
        const expectedState = merge(initialState,
            {error: 'GET_USER_NOTIFICATIONS_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle SEND_REVIEW_FAILURE', () => {
        const action = {type: SEND_REVIEW_FAILURE, error: 'SEND_REVIEW_FAILURE'};
        const expectedState = merge(initialState,
            {error: 'SEND_REVIEW_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

});