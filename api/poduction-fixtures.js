const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');
const Contact = require('./models/Contacts');

const dataBase = 'psychology-db';

mongoose.connect(`${config.db.url}/${dataBase}`);

const db = mongoose.connection;

const dropCollection = async (collectionName) => {
    try {
        await db.dropCollection(collectionName);
    } catch (e) {
        console.log(`Collection ${collectionName} did not present, skipping drop...`)
    }
};

const collections = ['users', 'contacts',
    'sections', 'questions',
    'answers', 'reviews',
    'reviewNotifications', 'userNotifications',
    'adminNotifications'];


db.once('open', async () => {
    collections.forEach(collectionName => (
        dropCollection(collectionName)
    ));

    const admin = await User.create({
        email: 'Aigulalikanova@gmail.com',
        displayName: 'Администратор',
        password: 'admin',
        role: 'admin',
        token: ''
    });


    const contacts = await Contact.create({
        phone: '0557-55-09-79',
        whatsapp: '0557-55-09-79',
        facebook: 'Facebook.com/insight.teens2',
        instagram: 'insight_teens',
        address: 'Какой-то адрес'
    });

    db.close();
});